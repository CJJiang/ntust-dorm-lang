# Ntust Dorm Repair Page Language Package

## Lang file
[lang-strings](./lang-strings.js)

## Web
[Website](https://dorm-ntust.tw)

## Contributor (sort by lang code)
* English(en) - `sheiun` `jessie`
* 繁體中文(zh-tw) - `sheiun`
